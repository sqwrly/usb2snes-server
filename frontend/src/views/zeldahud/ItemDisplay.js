"use strict";
var m = require('mithril')

export default class ItemDisplay {
    constructor(vnode) {
        this.data = vnode.attrs.data
        this.id = vnode.attrs.key
    }

    view() {
        var item = this.data.items[this.id]
        return m('td',
            m('img', {
                'src': `/img/alttp/${("000" + item.getImgNumber()).slice(-4)}.png`,
                'title': this.id,
                'class': item.hasItem() ? 'have-item' : 'need-item',
            })
        )
    }
}
