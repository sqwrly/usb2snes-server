"use strict";
var m = require("mithril");

import SnesStatus from '../models/SnesStatus';

export default class ViewStatusPage {
    constructor(vnode) {
    }

    view() {
        return [
            m('h1', 'SNES Status'),
            m('ul', [
                m('li',
                    SnesStatus.connected ?
                        [m('span.oi.oi-check'), " Connected"] :
                        [m('span.oi.oi-x'), " Disconnected"]
                ),
                SnesStatus.connected ?
                    [
                        m('li', `SD2SNES Version: ${SnesStatus.sd2snesVersion()}`),
                        m('li', `Current Game: ${SnesStatus.currentGame()}`),
                        m('li', `Patch loaded: ${SnesStatus.currentPatch == null ? "Unknown" : SnesStatus.currentPatch.name}`)
                    ] : ""
            ]),
            SnesStatus.connected ? [
                m('h2', 'Patch Loading'),
                Object.keys(SnesStatus.PATCHES).map(patch => [m('button.btn.btn-primary', {
                        onclick: SnesStatus.loadPatch.bind(SnesStatus, SnesStatus.PATCHES[patch]),
                        disabled: SnesStatus.currentPatch == SnesStatus.PATCHES[patch]
                    }, SnesStatus.PATCHES[patch].name), " "]
                )
            ] : ""
        ]
    }

    oninit() {
        SnesStatus.update()
    }
};
