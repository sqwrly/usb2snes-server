"use strict";
var m = require('mithril')
var Stream = require('mithril/stream')

import ApiHelper from './ApiHelper'
import SnesWebsocket from './SnesWebsocket'

function has(object, key) {
    return object ? hasOwnProperty.call(object, key) : false;
}

var GAME_NAME_LOROM = 0x7FC0
var GAME_NAME_HIROM = 0xFFC0

var SnesStatus = {
    connected: false,
    currentGame: Stream("Unknown"),
    sd2snesVersion: Stream("Unknown"),
    currentPatch: null,
    availablePorts: [],
    
    PATCHES: {
        // TODO: Maybe not the best place to put this
        SAVE_STATES: {
            name: "Save States",
            data: "UEFUQ0j8AAAGXUwbAgtSAAAMUgAAADGAABBDgIASQwAAFEPwABZDAACBIQAAgzEAAAtSAgASQwAAFEPxABZDAACBIQAAgzEBAAtSAgAQQ4E5FTGAABYhAAA5oQAAEkMAABRD8gAWQwAAC1ICACExAAAQQ4A7EkMAABRD9AAWQwIAC1ICAAIhAAAQQ4A4EkMAAhRD9CAWQwIAC1ICAAAArAMLUgAADFIAAAAxgAAWIQAAOaEAAAAArAMLUgAADFIAAAAxgAAQQwCAEkMAABRD8AAWQwAAgSEAAIMxAAALUgIAEkMAABRD8QAWQwAAgSEAAIMxAQALUgIAEEMBGBUxgAAWIQAAEkMAABRD8gAWQwAAC1ICACExAAAQQwAiEkMAABRD9AAWQwIAC1ICAAIhAAAQQwAEEkMAAhRD9CAWQwIAC1ICAAAAOARgYGBg4iDCEKICAKABAMACAPApwAMA8CTABADwH8AYAPAawBkA8BXAIgDwEL8AUPSZACHovwBQ9JkAIcro6MjANADwAoDIogEA4AEA8BHgCwDwDOAMAPAHv4BQ9J0AQujgDgDQ5MIwevpoK6viIK8QQgCvgFD0KQEJgI8AQgCvCiD8yQLQ+K8AUPSPACEAr4BQ9I8AQgBclCoAzCtQAGAAFwxQAGAAYJhQAGAAzjVQAGAAjLJQAGAAReVQAGAA3/gQYCBg2qBQAGAAgIxQAGAAwjMQECAQAAAQICAgwjBI2lqpagKPAQD84iCpAI8KIPyi2QHCIL8AAPzwK8/e/wDwCOjo6Ojo6IDq6Oi/AAD86OiPAiD8vwAA/OjojwQg/KkAAI8AIPzCMHr6aOIgrwog/PAdyQHQEmhorwog/BqPCiD8wiBo4iBoQBopD48KIPzCMK8GIPyPCCD8r/ArAI8GIPwvAiD8zwIg/PArrwYg/C8EIPzPBCD80ANM8QOvACD88BGqqQAAjwAg/Iop/wDQBkzxA0zVAcIgr97/AMnf+NAQowfJWYCQCckRgbAE4iCA4uIgrwog/NDaGo8KIPyvAAf5j4BQ9CkBjwBCAIsLwjBI2lriIK8MB/mPjFD0qQCPC0IAjwxCACBBAeIgoAAAu78ABfmfAFD06L8ABfmfAFD06MjAQADQ6OIgogEAvwAH+Z+AUPTo4AwA8PrgEADQ7eIgoAAAu78AQwCfABD06MjACwDQ8eB7APAK6Ojo6OigAACA4sIwoAMAr97/AMm3CfASyWCF8A3JVUDwCMnia/ADoJcA9AD8q6tMKwbCMKnw9Y8gIACpAACPIiAAqQAAjyQgAKkABY8mIADiIKkEjyggAKkFjykgAMIgO48AYPTiIPQAAKurwiAgQgFMSQRM1QEgPwHCIK/e/wDJ3/jQEKMHyVmAkAnJEYGwBOIggN/iIK8KIPzQ1xqPCiD8rwAH+SkBjwBCAIsLwjBI2lr0AACrq6CvAEykA8IwrwBg9Bv0AACrq8IwIEABoAAAWqKtBMIgvwAA/PBNz97/APAK6Ojo6Ojo6OiA6HrIWujovwAA/Kjo6L8AAPziIEirwiDo6L8AAPzayUAhkAzJRCGwB6riIL8AAADiIJkAAPr0AACrq+jogKviIHrQAEwFBo1k9QIAAEAhD/71AgAAQCHUtvv/fgBAIYgy+/9+AEAhnpX7/34AQCEnov//fgBAIVpb//9+AEAhfcT+/38AQiF9Lf7/fwBCIZL+/v9+AEIhwkz+/34AQiFpZf7/fgBCIbcJ/v9+AEIhYIX+/34AQiFVQP7/fgBCIeJr/v9+AEIhCgYCAAAAQiGaWwIAAABCIREL+/9+AEIhfXz8/34AQiHMKwAAAABAIRcMAAAAAEAhYJgAAAAAQCHONQAAAABAIYyyBgAAAEAhReUGAAAAQCF/rQAAAABDIZoRrRwAAEIhCmFrHQAAQiGIOn5/fwBCIX0KdgYAAEAhLIz4/34AQCEM9R4AAABDIb5c/xsAAEAhJRsMCwAAQyEvFBsDAAAAAC8UHAMAAAAA5sokBgAAQCHmyiUGAABBIdBaCf9/AEIhSmEKAAAAQiHdJHYBfgBAIQAAAAAAAAAA4iCiAACbvwAQ9J0AQ+jIwAsA0PLgewDwCujo6OjooAAAgONMQwHCILkAAPAnyMjJAIAp/3+qiQAQ8AYp/++q4iC5AADIyLAGnwAAAIDYvwAAAIDSu3wCAPwgAAAMAQEQICAgAAAAAAAA0AAAAAVcAAD8YEVPRg==",
            type: "hook",
        },
    },
    
    getAvailablePorts: function() {
        SnesWebsocket.sendSingle({
            "Opcode": "DeviceList",
        }).then((data) => {
            console.log("Updated")
            this.availablePorts = data.Results
        })
    },
    
    update: function() {
        this.connected = true
        this.getCurrentGame(GAME_NAME_LOROM)
    },

    getCurrentGame: function(address) {
        console.log("Called getCurrentGame")
        SnesWebsocket.sendSingle({
            "Opcode": "Info",
            "Space": "SNES",
        }).then((data) => {
            console.log("Got current game")
            if (data.Results.length >= 4)  {
                this.currentGame(data.Results[2])
                this.sd2snesVersion(`${data.Results[0]} (0x${data.Results[1]})`)
            }
        })
    },
    
    loadPatch: function(patch) {
        var decoded_patch = this._base64ToArrayBuffer(patch.data)
        var patch_size = decoded_patch.byteLength

        SnesWebsocket.send({
            "Opcode": "PutIPS",
            "Space": "SNES",
            "Flags": null,
            "Operands": [patch.type, patch_size.toString(16)]
        })

        var CHUNK_SIZE = 1024
        for (var i = 0; i < patch_size; i += CHUNK_SIZE) {
            SnesWebsocket.websocket.send(decoded_patch.slice(i, i + CHUNK_SIZE))
        }

        this.currentPatch = patch
    },

    _base64ToArrayBuffer: function(base64) {
        var binary_string = window.atob(base64)
        var len = binary_string.length
        var data = new Uint8Array(len)
        for (var i = 0; i < len; ++i) {
            data[i] = binary_string.charCodeAt(i)
        }

        return data
    }
}

module.exports = SnesStatus
