"use strict";
var m = require("mithril");

import SnesWebsocket from './SnesWebsocket'

export default class WramArea {
    constructor(address, size) {
        this.has_loaded = false
        this.raw = new Uint8Array(size)
        this.address = address
    }

    update() {
        SnesWebsocket.waitForMessageCallback().then(() => {
            SnesWebsocket.message_callback = (data) => {
                SnesWebsocket.message_callback = null
                var data_reader = new FileReader()
                data_reader.onload = (event) => {
                    this.raw = new Uint8Array(event.target.result)
                    m.redraw()
                }
                data_reader.readAsArrayBuffer(data)
            }

            SnesWebsocket.send({
                "Opcode": "GetAddress",
                "Space": "SNES",
                "Flags": null,
                "Operands": [this.address.toString(16), this.raw.length.toString(16)]
            })
        })
    }

    updateRawData(b64_data) {
        var binary_string = window.atob(b64_data)
        var len = binary_string.length

        if (len != this.raw.length) {
            // TODO... what?
            return
        }

        for (var i = 0; i < len; ++i) {
            this.raw[i] = binary_string.charCodeAt(i)
        }
    }
}
