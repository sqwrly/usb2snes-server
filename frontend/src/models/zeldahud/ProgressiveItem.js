"use strict";

import ItemBase from './ItemBase'

export default class ProgressiveItem extends ItemBase {
    constructor(wram, address, progression) {
        super(wram, address)

        this.progression = progression
    }

    getImgNumber() {
        return this.progression[Math.max(this.getValue() - 1, 0)]
    }

    hasItem() {
        return this.getValue() > 0
    }
}
