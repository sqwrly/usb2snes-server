"use strict";

import ItemBase from './ItemBase'

export default class SingleItem extends ItemBase {
    constructor(wram, address, image) {
        super(wram, address)

        this.image = image
    }

    getImgNumber() {
        return this.image
    }

    hasItem() {
        return this.getValue() > 0
    }
}
